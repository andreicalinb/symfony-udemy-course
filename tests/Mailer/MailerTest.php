<?php

namespace App\Tests\Mailer;

use App\Entity\User;
use App\Mailer\Mailer;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

/**
 * @internal
 * @covers \App\Mailer\Mailer
 */
class MailerTest extends TestCase
{
    public function testConfirmationEmail(): void
    {
        $user = new User();
        $user->setEmail('john@doe.com');

        $swiftMailerMock = $this->getMockBuilder(\Swift_Mailer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $swiftMailerMock->expects($this->once())
            ->method('send');

        $twigMock = $this->getMockBuilder(Environment::class)
            ->disableOriginalConstructor()
            ->getMock();
        $twigMock->expects($this->once())
            ->method('render')
            ->with('email/registration.html.twig', ['user' => $user])
            ->willReturn('This is a message body');

        /** @var Environment $twigMock */
        /** @var \Swift_Mailer $swiftMailerMock */
        $mailer = new Mailer('me@domain.com', $twigMock, $swiftMailerMock);

        try {
            $mailer->sendConfirmationEmail($user);
        } catch (\Exception $ex) {
            $this->fail($ex->getMessage());
        }
    }
}
