<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * @Route("/blog")
 * @psalm-suppress PropertyNotSetInConstructor
 */
class BlogController extends AbstractController
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Environment
     */
    private $twig;

    public function __construct(RouterInterface $router, SessionInterface $session, Environment $twig)
    {
        $this->router = $router;
        $this->session = $session;
        $this->twig = $twig;
    }

    /**
     * @Route("/", name="blog_index")
     */
    public function index(): Response
    {
        // No access if we're extending the AbstractController, but only when extending the Controller
//        $this->get('app.greeting');

        /** @noinspection PhpUnhandledExceptionInspection */
        $html = $this->twig->render('blog/index.html.twig', ['posts' => $this->session->get('posts')]);

        return new Response($html);
    }

    /**
     * @Route("/add", name="blog_add")
     */
    public function add(): RedirectResponse
    {
        $posts = $this->session->get('posts');
        $posts[uniqid()] = [
            'title' => 'A random title '.rand(1, 500),
            'text' => 'Some random text nr '.rand(1, 500),
            'date' => new \DateTime(),
        ];
        $this->session->set('posts', $posts);

        return new RedirectResponse($this->router->generate('blog_index'));
    }

    /**
     * @Route("/show/{id}", name="blog_show")
     */
    public function show(string $id): Response
    {
        $posts = $this->session->get('posts');

        if (!$posts || !isset($posts[$id])) {
            throw new NotFoundHttpException('Post not found');
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $html = $this->twig->render('blog/post.html.twig', ['id' => $id, 'post' => $posts[$id]]);

        return new Response($html);
    }
}
