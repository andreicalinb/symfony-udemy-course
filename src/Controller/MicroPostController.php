<?php

namespace App\Controller;

use App\Entity\MicroPost;
use App\Entity\User;
use App\Form\MicroPostType;
use App\Repository\MicroPostRepository;
use App\Repository\UserRepository;
use App\Security\MicroPostVoter;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Twig\Environment;

/**
 * @Route("/micro-post")
 */
class MicroPostController
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var MicroPostRepository
     */
    private $microPostRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Environment
     */
    private $twig;

    public function __construct(AuthorizationCheckerInterface $authChecker, EntityManagerInterface $entityManager, Environment $twig, FormFactoryInterface $formFactory, MicroPostRepository $microPostRepository, RouterInterface $router, SessionInterface $session)
    {
        $this->authChecker = $authChecker;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->microPostRepository = $microPostRepository;
        $this->router = $router;
        $this->session = $session;
        $this->twig = $twig;
    }

    /**
     * @Route("/", name="micro_post_index")
     */
    public function index(TokenStorageInterface $tokenStorage, UserRepository $userRepo): Response
    {
        /** @var TokenInterface $token */
        $token = $tokenStorage->getToken();
        /** @var null|User $currentUser */
        $currentUser = $token->getUser();

        $usersToFollow = [];
        if ($currentUser instanceof User) {
            $posts = $this->microPostRepository->findAllByUsers($currentUser->getFollowing());

            $usersToFollow = 0 === \count($posts) ? $userRepo->findAllWitMoreThan5PostsExceptUser($currentUser) : [];
        } else {
            $posts = $this->microPostRepository->findBy([], ['time' => 'DESC']);
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $html = $this->twig->render(
            'micro-post/index.html.twig',
            ['posts' => $posts, 'usersToFollow' => $usersToFollow]
        );

        return new Response($html);
    }

    /**
     * @Route("/edit/{id}", name="micro_post_edit")
     * @Security("is_granted('edit', post)", message="Access denied")
     */
    public function edit(MicroPost $post, Request $request): Response
    {
        // If you extend the base Controller class, then you can
        // $this->denyUnlessGranted(MicroPostVoter::EDIT, $post);.

        // You can also use the next annotation.
        //  if (!$this->authChecker->isGranted(MicroPostVoter::EDIT, $post)) {
        //    throw new UnauthorizedHttpException('');
        //  }

        $form = $this->formFactory->create(MicroPostType::class, $post);
        //Validation happens in the next line
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($post);
            $this->entityManager->flush();

            return new RedirectResponse($this->router->generate('micro_post_index'));
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return new Response($this->twig->render('micro-post/add.html.twig', ['form' => $form->createView()]));
    }

    /**
     * @Route("/delete/{id}", name="micro_post_delete")
     * @Security("is_granted('delete', post)", message="Access denied")
     */
    public function delete(MicroPost $post): Response
    {
        if (!$this->authChecker->isGranted(MicroPostVoter::DELETE, $post)) {
            throw new UnauthorizedHttpException('');
        }

        $this->entityManager->remove($post);
        $this->entityManager->flush();

        /** @var FlashBag $flashes */
        $flashes = $this->session->getBag('flashes');
        $flashes->add('notice', 'Micro post was deleted');

        return new RedirectResponse($this->router->generate('micro_post_index'));
    }

    /**
     * @Route("/add", name="micro_post_add")
     * @Security("is_granted('ROLE_USER')")
     */
    public function add(Request $request, TokenStorageInterface $tokenStorage): Response
    {
        /** @var TokenInterface $token */
        $token = $tokenStorage->getToken();
        /** @var User $user */
        $user = $token->getUser();

        $post = new MicroPost();
        $post->setUser($user);

        $form = $this->formFactory->create(MicroPostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($post);
            $this->entityManager->flush();

            return new RedirectResponse($this->router->generate('micro_post_index'));
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return new Response($this->twig->render('micro-post/add.html.twig', ['form' => $form->createView()]));
    }

    /**
     * @Route("/user/{username}", name="micro_post_user")
     */
    public function userPosts(User $user): Response
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $html = $this->twig->render(
            'micro-post/user-posts.html.twig',
            [
                // Posts can be taken from the database or from the User entity
                'posts' => $this->microPostRepository->findBy(['user' => $user], ['time' => 'DESC']),
                //                'posts' => $user->getPosts(),
                'user' => $user,
            ]
        );

        return new Response($html);
    }

    /**
     * @Route("/{id}", name="micro_post_post")
     */
    public function post(MicroPost $post): Response
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return new Response($this->twig->render('micro-post/post.html.twig', ['post' => $post]));
    }
}
