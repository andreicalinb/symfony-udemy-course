<?php

namespace App\Controller;

use App\Entity\User;
use App\Event\UserRegisterEvent;
use App\Form\UserType;
use App\Security\TokenGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @psalm-suppress TooManyArguments
     * @psalm-suppress InvalidArgument
     * @Route("/register", name="user_register")
     */
    public function register(EventDispatcherInterface $eventDispatcher, Request $request, TokenGenerator $tokenGenerator, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encodedPassword = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encodedPassword);

            $confirmationToken = $tokenGenerator->getRandomSecureToken(30);
            $user->setConfirmationToken($confirmationToken);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $userRegisterEvent = new UserRegisterEvent($user);
            /** @noinspection PhpMethodParametersCountMismatchInspection */
            /** @noinspection PhpParamsInspection */
            $eventDispatcher->dispatch(UserRegisterEvent::NAME, $userRegisterEvent);

            $this->redirect('micro_post_index');
        }

        return $this->render('register/register.html.twig', ['form' => $form->createView()]);
    }
}
