<?php

namespace App\Security;

class TokenGenerator
{
    private const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    public function getRandomSecureToken(int $length): string
    {
        $maxNumber = \strlen(self::ALPHABET);
        $token = '';

        try {
            for ($i = 0; $i < $length; ++$i) {
                $token .= self::ALPHABET[random_int(0, $maxNumber - 1)];
            }
        } catch (\Exception $ex) {
        }

        return $token;
    }
}
