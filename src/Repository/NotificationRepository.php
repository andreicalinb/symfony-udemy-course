<?php

namespace App\Repository;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method null|Notification find($id, $lockMode = null, $lockVersion = null)
 * @method null|Notification findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function findUnseenByUser(User $user): int
    {
        $qb = $this->createQueryBuilder('n');

        try {
            return $qb->select('count(n)')
                ->where('n.user = :user')
                ->andWhere('n.seen = 0')
                //Doctrine automatically knows to get the id if you pass it an entity instance
                ->setParameter('user', $user)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception $ex) {
            return 0;
        }
    }

    public function markAllAsReadByUser(User $user): void
    {
        $qb = $this->createQueryBuilder('n');

        $qb->update('App\Entity\Notification', 'n')
            ->set('n.seen', true)
            ->where('n.user =s :user')
            //Doctrine automatically knows to get the id if you pass it an entity instance
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }
}
