<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeNotificationRepository")
 */
class LikeNotification extends Notification
{
    /**
     * @var MicroPost
     * @ORM\ManyToOne(targetEntity="App\Entity\MicroPost")
     */
    private $microPost;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $likedBy;

    public function __construct()
    {
        parent::__construct();
        $this->likedBy = new User();
        $this->microPost = new MicroPost();
    }

    public function getMicroPost(): MicroPost
    {
        return $this->microPost;
    }

    public function setMicroPost(MicroPost $microPost): void
    {
        $this->microPost = $microPost;
    }

    public function getLikedBy(): User
    {
        return $this->likedBy;
    }

    public function setLikedBy(User $user): void
    {
        $this->likedBy = $user;
    }
}
