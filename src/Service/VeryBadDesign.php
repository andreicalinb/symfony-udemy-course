<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class VeryBadDesign implements ContainerAwareInterface
{
    /**
     * @required
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        //Need to use when services.yaml:_defaults.public = false
        //Basically, it manually puts a private service into the container - bad practice
//        $container->get(Greeting::class);

        //Use a service alias that is public
//        $container->get('app.greeting');
    }
}
