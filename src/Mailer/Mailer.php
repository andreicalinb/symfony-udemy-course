<?php

namespace App\Mailer;

use App\Entity\User;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Mailer
{
    /**
     * @var string
     */
    private $emailFrom;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    public function __construct(string $emailFrom, Environment $twig, \Swift_Mailer $mailer)
    {
        $this->emailFrom = $emailFrom;
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @throws LoaderError | RuntimeError |SyntaxError
     */
    public function sendConfirmationEmail(User $user): void
    {
        try {
            $body = $this->twig->render('email/registration.html.twig', ['user' => $user]);

            $message = (new \Swift_Message())
                ->setSubject('Welcome to the micro-post app')
                ->setFrom($this->emailFrom)
                ->setTo($user->getEmail())
                ->setBody($body, 'text/html');

            $this->mailer->send($message);
        } catch (\Exception $ex) {
        }
    }
}
