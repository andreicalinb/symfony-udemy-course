# What

This is the support code for [this](https://www.udemy.com/course/learn-symfony-4-hands-on-creating-a-real-world-application/) course.

# Why

The scope was to improve my Symfony skill.

# How to install and configure
0. Make sure you have Docker and docker-compose installed (I use `Docker version 17.05.0-ce` and `docker-compose version 1.22.0`)
1. Close the repository in `/var/www/html/symfony-udemy-course`
2. Create the containers `docker-compose up -d`
3. Install BE dependencies `docker exec -it symfony-udemy-course-php-fpm composer install`
4. Front page should work `http://localhost:8000/`

# BE cheatsheet
  * Install BE dependencies changes: `docker exec -it symfony-udemy-course-php-fpmcomposer install`
  * Run symfony console: `docker exec -it symfony-udemy-course-php-fpm php bin/console`
  * Generate migrations: `docker exec -it symfony-udemy-course-php-fpm php bin/console make:migration`
  * Apply migrations: `docker exec -it symfony-udemy-course-php-fpm php bin/console doctrine:migrations:migrate`
  * Clear cache: `docker exec -it symfony-udemy-course-php-fpm php bin/console cache:clear`

# FE cheatsheet
  * Install BE dependencies changes: `docker exec -it symfony-udemy-course-php-fpmyarn install`
  * Compile frontend: `docker exec -it symfony-udemy-course-php-fpmyarn encore dev`

# Code quality
  * PHP cs fixer - check for errors: `docker exec -it symfony-udemy-course-php-fpm php vendor/bin/php-cs-fixer fix -v --dry-run`
  * PHP cs fixer - automatically fix errors: `docker exec -it symfony-udemy-course-php-fpm php vendor/bin/php-cs-fixer fix`
  * PHPStan - check for bugs: `docker exec -it symfony-udemy-course-php-fpm php vendor/bin/phpstan analyse --no-progress --no-interaction`
  * Psalm - check for bugs: `docker exec -it symfony-udemy-course-php-fpm php vendor/bin/psalm --no-progress --show-info=false src/`
  * All: `docker exec -it symfony-udemy-course-php-fpm php vendor/bin/php-cs-fixer fix && docker exec -it symfony-udemy-course-php-fpm php vendor/bin/phpstan analyse --no-progress --no-interaction && docker exec -it symfony-udemy-course-php-fpm php vendor/bin/psalm --no-progress --show-info=false src/`
